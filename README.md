# openGauss社区
[English](./README.md) | 简体中文



欢迎来到openGauss社区。


## 介绍

代码仓Community保持了关于openGauss社区的所有信息，包括社区治理、社区活动、开发者贡献指南、沟通交流指南等内容。 


## 如何在本代码仓贡献

当openGauss社区有信息刷新时，这里的文档也需要同步刷新。很感谢您愿意帮助刷新内容。

请阅读[如何贡献](/CONTRIBUTING.md)获得帮助。

