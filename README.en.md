# openGauss Community
English | [简体中文](./README_cn.md)

Welcome to openGauss Community.


## Introduction

The Community repository is to store all the information about openGauss Community, including governance, how to contribute, communications and etc. 

## How to contribute

When openGauss community is updated, the information should be updated as well here. If you would like to help update the information in this repo, you are very appreciated. 

Please read [How to contribute](CONTRIBUTING.md) to get detailed guidance.
